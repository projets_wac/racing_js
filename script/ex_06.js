window.onload = function() {


function Hero(nom,classe,intelligence,force){
	this.nom = nom[0].toUpperCase() + nom.substr(1);//met la première lettre en majuscule.
	this.classe =classe;
	this.intelligence = intelligence;
	this.force = force;
}

Hero.prototype.toString = function() {
	return "Je suis " + this.nom + " le "+ this.classe + ", j'ai " + this.intelligence + " points d'intelligence et " + this.force + " points de force" + " !<br />";
	}

	var mage = new Hero("amadeus","mage","10","3");
	var guerrier = new Hero("pontius","guerrier","3","10");
	mage.toString();
	guerrier.toString();
	document.getElementsByTagName("footer")[0].children[0].innerHTML = mage + guerrier;
};